/* ART URL Parser
	Depends on <Urlmon.h> Urlmon.lib,
	windows
	<sstream>
	and malloc

	TODOS:
	make it depend on sml_defaults.h
	make it depend on msl_alloc.h
	make it depend on msl_string.h
	privatize functions that not need to be exposed

*/

#ifndef MSL_DOMTREE_H
#define MSL_DOMTREE_H
//##########[ Header Implementation ]##########

#include <Urlmon.h>
#include <sstream>


#define MSL_DOMTREE_MAXQUERYRESULTS 2048


enum mslHtmlElementTypes
{
	Special,
	Paired,
	Empty,
};

enum mslDomTagTypes
{
	OpenTag,
	ClosingTag,
	FlatTag,
};



struct mslElementType
{
	char* Name;
	mslHtmlElementTypes Type;
};

struct mslMemarena
{
	char* Data;
	int WritePointer;
	int AllocationSize;
	bool StreamWrite;
};

struct mslUrldata
{
	char* Data;
	int Size;
	int Error;
};

struct mslTreeNode
{
	mslTreeNode* ParentNode;
	mslTreeNode* ChildNode;
	mslTreeNode* NextSiblingNode;
	char* InnerText;
	char* TagName;
	mslHtmlElementTypes Type;
};

struct mslQueryNode
{
	bool Found;
	mslTreeNode* Node[MSL_DOMTREE_MAXQUERYRESULTS];
	int Count;
};

struct mslDomtree
{
	mslTreeNode* Node;
	mslMemarena MemArena;
};

namespace MslDomTree
{
	mslUrldata DownloadUrl(char* url);
	mslDomtree ParseDom(mslUrldata data);
}



#endif

#ifdef MSL_DOMTREE_IMPLEMENTATION
#ifndef _MSL_DOMTREE_IMPLEMENTED_GUARD
//##########[ Implementation ]##########



namespace MslDomTree
{
	void CreateAreanAllocator(mslMemarena* arena, int arenaSize)
	{
		arena->AllocationSize = arenaSize;
		arena->Data = (char*)malloc(arenaSize);
		arena->WritePointer = 0;
	}

	void FreeAreanAllocator(mslMemarena* arena)
	{
		free(arena->Data);
		arena->Data = 0;
		arena->AllocationSize = 0;
		arena->WritePointer = 0;
	}

	char* ArenaAlloc(int bytes, mslMemarena* arena)
	{
		if (arena->StreamWrite)
			return 0;
		if (arena->AllocationSize < arena->WritePointer + bytes)
			return 0;

		char* result = arena->Data + arena->WritePointer;
		arena->WritePointer += bytes;

		for (int i = 0; i < bytes; ++i)
		{
			result[i] = 0;
		}
		return result;
	}

	char* ArenaBeginStream(mslMemarena* arena)
	{
		char* result = arena->Data + arena->WritePointer;
		arena->StreamWrite = true;

		return result;
	}

	void ArenaEndStream(mslMemarena* arena)
	{
		char* result = arena->Data + arena->WritePointer;
		*result = '\0';
		arena->WritePointer++;
		arena->StreamWrite = false;
	}

	void ArenaStream(char c, mslMemarena* arena)
	{
		char* result = arena->Data + arena->WritePointer;
		*result = c;
		arena->WritePointer++;
	}

	char* StreamText(char*& p, char exitChar, bool readOnePast, mslMemarena* arena)
	{

		char* result = ArenaBeginStream(arena);
		while (*p != 0 && *p != exitChar)
		{
			ArenaStream(*p, arena);
			p++;
		}
		if (readOnePast == true)
		{
			ArenaStream(*p, arena);
			p++;
		}
		ArenaEndStream(arena);
		//printf(result);
		return result;
	}


	static char* SkipWhitespace(char*& p)
	{
		while (*p && (*p == ' ' || *p == '\t' || *p == '\n' || *p == '\r')) {
			p++;
		}

		return p;
	}

	static char* SkipToNextTag(char*& p)
	{
		while (*p && *p != '>') {
			p++;
		}
		p++;

		return p;
	}

	static bool CompareStrings(char* left, char* right)
	{
		while (*left || *right)
		{
			if (*left++ != *right++) return false;
		}
		if (*left != *right) return false;

		return true;
	}

	static bool StringBeginsWith(char* left, char* right)
	{
		while (*left || *right)
		{
			if (*left != *right)
			{
				if (*right == 0) return true;
				return false;
			}
			*left++;
			*right++;
		}
		if (*left != *right) return false;

		return true;
	}

	bool IsSelfClosingTag(char* rPointer)
	{
		bool result = StringBeginsWith(rPointer, "<br>");
		if(result == true) return result;
		result = StringBeginsWith(rPointer, "<BR>");
		if (result == true) return result;
		result = StringBeginsWith(rPointer, "<img");
		if (result == true) return result;
		result = StringBeginsWith(rPointer, "<IMG");
		if (result == true) return result;

		return false;
	}

	mslUrldata DownloadUrl(char* url)
	{

		mslUrldata result = {};

		IStream* stream = 0;
		const wchar_t* destFile = L"myfile.txt";
		HRESULT dls = URLOpenBlockingStream(0, url, &stream, 0, 0);
		if (dls != 0)
		{
			result.Error = 404;
			return result;
		}

		char szBuffer[0x100];
		DWORD dwGot;
		HRESULT hr = NOERROR;

		result.Data = (char*)malloc(1024);
		int allocSize = 1024;
		int write = 0;


		do
		{
			hr = stream->Read(szBuffer, sizeof(szBuffer) - 1, &dwGot);

			memcpy(&result.Data[write], szBuffer, dwGot);
			write += dwGot;
			result.Size = write;
			if (write + (int)dwGot >= allocSize)
			{
				allocSize += 1024;
				result.Data = (char*)realloc(result.Data, allocSize);
			}

		} while (SUCCEEDED(hr) && hr != S_FALSE);

		stream->Release();
		result.Data[result.Size] = '\0';

		return result;
	}


	mslTreeNode* ParseNode(char* rPointer, mslMemarena* arena)
	{

		mslTreeNode* root = (mslTreeNode*)ArenaAlloc(sizeof(mslTreeNode), arena);
		root->Type = mslHtmlElementTypes::Paired;
		root->TagName = StreamText(rPointer, '>', true, arena);
		root->ChildNode = 0;
		root->NextSiblingNode = 0;
		root->ParentNode = 0;

		mslTreeNode* currentNode = root;

		while (*rPointer)
		{
			SkipWhitespace(rPointer);
			if (*rPointer == '<')
			{

				if (*(rPointer) == '!')
				{
					// Comments and special tags, skipp these...
					SkipToNextTag(rPointer);
				}
				else
					if (*(rPointer + 1) == '/')
					{
						// Closing Tag, back out to previus tag
						SkipToNextTag(rPointer);
						if (currentNode->ParentNode != 0)
						{
							currentNode = currentNode->ParentNode;
						}
					}
					else if(IsSelfClosingTag(rPointer))
					{
						// Add selfclosing tags
						mslTreeNode* newNode = (mslTreeNode*)ArenaAlloc(sizeof(mslTreeNode), arena);
						newNode->Type = mslHtmlElementTypes::Empty;
						newNode->TagName = StreamText(rPointer, '>', true, arena);
						newNode->ChildNode = 0;
						newNode->NextSiblingNode = 0;
						newNode->ParentNode = currentNode;

						if (currentNode->ChildNode == 0)
						{
							currentNode->ChildNode = newNode;
						}
						else
						{
							// Find free sibling
							mslTreeNode* sibling = currentNode->ChildNode;
							while (sibling->NextSiblingNode != 0)
							{
								sibling = sibling->NextSiblingNode;
							}
							sibling->NextSiblingNode = newNode;
						}

					}
					else
					{
						// Opening tag
						mslTreeNode* newNode = (mslTreeNode*)ArenaAlloc(sizeof(mslTreeNode), arena);
						newNode->Type = mslHtmlElementTypes::Paired;
						newNode->TagName = StreamText(rPointer, '>', true, arena);
						newNode->ChildNode = 0;
						newNode->NextSiblingNode = 0;
						newNode->ParentNode = currentNode;
						if (currentNode->ChildNode == 0)
						{
							currentNode->ChildNode = newNode;
						}
						else
						{
							// Find free sibling
							mslTreeNode* sibling = currentNode->ChildNode;
							while (sibling->NextSiblingNode != 0)
							{
								sibling = sibling->NextSiblingNode;
							}
							sibling->NextSiblingNode = newNode;
						}
						currentNode = newNode;
					}

			}
			else
			{
				// Text tag (Inner html)
				mslTreeNode* textNode = (mslTreeNode*)ArenaAlloc(sizeof(mslTreeNode), arena);
				textNode->Type = mslHtmlElementTypes::Paired;
				bool isText = true;
				if (CompareStrings(currentNode->TagName, "<script>"))
				{
					textNode->TagName = "InnerScript";
					isText = true;
				}
				else
				{
					textNode->TagName = "InnerText";
				}

				textNode->ChildNode = 0;
				textNode->NextSiblingNode = 0;
				textNode->ParentNode = currentNode;
				if (currentNode->ChildNode == 0)
				{
					currentNode->ChildNode = textNode;
				}
				else
				{
					// Find free sibling
					mslTreeNode* sibling = currentNode->ChildNode;
					while (sibling->NextSiblingNode != 0)
					{
						sibling = sibling->NextSiblingNode;
					}
					sibling->NextSiblingNode = textNode;
				}
				textNode->InnerText = StreamText(rPointer, '<', false, arena);

				if(isText == true) printf("%s\n", textNode->InnerText);

			}


		}

		return root;
	}

	mslDomtree ParseDom(mslUrldata data)
	{
		mslDomtree tree = {};
		CreateAreanAllocator(&tree.MemArena, 1024 * 1024 * 32); // 32 mb

		tree.Node = ParseNode(data.Data, &tree.MemArena);

		return tree;
	}

	void FreeDomTree(mslDomtree tree)
	{
		FreeAreanAllocator(&tree.MemArena);

	}



	void QueryElement(mslTreeNode* node, char* tagName, mslQueryNode* result)
	{
		if (StringBeginsWith(node->TagName, tagName))
		{
			result->Found = true;
			if (result->Count < MSL_DOMTREE_MAXQUERYRESULTS)
			{
				result->Node[result->Count++] = node;
			}
		}

		if (node->ChildNode != 0)
		{
			QueryElement(node->ChildNode, tagName, result);
		}

		if (node->NextSiblingNode != 0)
		{
			QueryElement(node->NextSiblingNode, tagName, result);
		}
	}
}



#define _MSL_DOMTREE_IMPLEMENTED_GUARD
#undef MSL_DOMTREE_IMPLEMENTATION
#endif
#endif


/*
    Changelog

    231226  File created
	240106  Added support for selfclosing tags
			Added free function for domtree mem arena


    
*/
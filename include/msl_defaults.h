/*
    My Standard Library
    Version 1.0

*/

#ifndef MSL_DEFAULTS_H
#define MSL_DEFAULTS_H
//####################[ Header Implementation ]####################



#if DEBUG_BUILD
#define ASSERT(Expression, Message) if(!(Expression))printf("ASSERTION HALT\nin file %s\nin function %s On line: %d\n%s\n", __FILE__, __FUNCTION__, __LINE__,  Message);if(!(Expression)) {*(int *)0 = 0;}
#else
#define ASSERT(Expression, Message)
#endif
// Todo use _Static_assert for gcc/clang 
#define STATICASSERT static_assert



typedef char int8;
typedef unsigned char uint8;
typedef short int16;
typedef unsigned short uint16;
typedef int int32;
typedef unsigned int uint32;
typedef long long int64;
typedef unsigned long long uint64;
typedef float float32;
typedef double float64;

STATICASSERT(sizeof(int8) == 1, "Expected size of 8bit integer to be 1 byte");
STATICASSERT(sizeof(uint8) == 1, "Expected size of 8bit integer to be 1 byte");
STATICASSERT(sizeof(int16) == 2, "Expected size of 16bit integer to be 2 byte");
STATICASSERT(sizeof(uint16) == 2, "Expected size of 16bit integer to be 2 byte");
STATICASSERT(sizeof(int32) == 4, "Expected size of 32bit integer to be 4 byte");
STATICASSERT(sizeof(uint32) == 4, "Expected size of 32bit integer to be 4 byte");
STATICASSERT(sizeof(int64) == 8, "Expected size of 64bit integer to be 8 byte");
STATICASSERT(sizeof(uint64) == 8, "Expected size of 64bit integer to be 8 byte");
STATICASSERT(sizeof(float32) == 4, "Expected size of 32bit float to be 4 byte");
STATICASSERT(sizeof(float64) == 8, "Expected size of 64bit float to be 8 byte");



#endif

#ifdef MSL_DEFAULTS_IMPLEMENTATION
#ifndef _MSL_DEFAULTS_IMPLEMENTED_GUARD
//##########[ Implementation ]##########





#define _MSL_DEFAULTS_IMPLEMENTED_GUARD
#undef MSL_DEFAULTS_IMPLEMENTATION
#endif
#endif

/*
    Changelog

    231226  File created
            Default types added
            Assert and Static assert

    
*/
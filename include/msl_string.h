#ifndef MSL_STRING_H
#define MSL_STRING_H
//##########[ Header Implementation ]##########

#include "msl_defaults.h"


enum mslStringEncodings
{
    ASCII,      //  Not implemented
    UTF8,       //  Not implemented
    UTF16       //  Not implemented
};

struct mslString
{
    uint32                  Lenght;
    mslStringEncodings      Encoding;
    uint8                   Data;
};



namespace MslString
{
    static int32            GetLenght(char* msg);
    static bool             Compare(char* msg);
    static bool             BeginsWith(char* sourceMsg, char* comparer);
}

#endif

#ifdef MSL_STRING_IMPLEMENTATION
#ifndef _MSL_STRING_IMPLEMENTED_GUARD
//##########[ Implementation ]##########


namespace MslString
{
    //  Count lenght of a zero terminated string
    static int32 GetLenght(char* msg)
    {
        int index = 0;
        while (msg[index]) index++;
        return index;
    }



    //  Compare two strings with each other
    static bool Compare(char* leftMsg, char* rightMsg)
    {
        while (*leftMsg || *rightMsg)
		{
			if (*leftMsg++ != *rightMsg++) return false;
		}
		if (*leftMsg != *rightMsg) return false;

		return true;
    }



    //  Check if left message is beginning with right message
    static bool BeginsWith(char* sourceMsg, char* comparer)
	{
		while (*sourceMsg || *comparer)
		{
			if (*sourceMsg != *comparer)
			{
				if (*comparer == 0) return true;
				return false;
			}
			*sourceMsg++;
			*comparer++;
		}
		if (*sourceMsg != *comparer) return false;

		return true;
	}
}


#define _MSL_STRING_IMPLEMENTED_GUARD
#undef MSL_STRING_IMPLEMENTATION
#endif
#endif



/*
    Changelog

    240115  File created
        

    
*/
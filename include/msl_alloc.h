/*
    My Standard Library
    Version 1.0
    Depends on msl_defaults.h

*/
#ifndef MSL_ALLOC_H
#define MSL_ALLOC_H
//##########[ Header Implementation ]##########
#include "msl_defaults.h"


#define MSL_CORRUPTIONGUARD 4275878552     //#FEDCBA98 

enum mslAllocationStates
{
    Free,
    Occupied,
    End,
};


static int _MSL_GLOBAL_ALLOCINDEX = 0;

struct mslAllocator
{
    uint32*                 Start;
    uint32*                 WritePointer;
    int32                   Size;
    int32                   Stride;
    int32                   UsedSpace;
    uint32                  FragmentedSpace;
    uint32                  HeaderSpace;
};

struct mslAllocationHeader
{
    uint32                  CorruptionGuard;    //  Always #FEDCBA98
    uint32                  Identifyer;
    mslAllocationHeader*    PreviousBlock;
    mslAllocationHeader*    NextBlock;
    void*                   Data;
    uint32                  BlockSize;
    uint32                  FragmentedSpace;    //  Unused space
    mslAllocationStates     State;
};

struct mslMemBlock
{
    void*                   Data;
    uint32                  BlockSize;
};

namespace MslAlloc
{
    mslAllocator            Create(void* memory, uint32 size);
    mslMemBlock             Alloc(mslAllocator* allocator, uint32_t blockSize);
    void                    Free(mslAllocator* allocator, void* block);
}


#endif

#ifdef MSL_ALLOC_IMPLEMENTATION
#ifndef _MSL_ALLOC_IMPLEMENTED_GUARD
//##########[ Implementation ]##########



// Private functions
namespace __MslAlloc
{
	int Align(int bytes)
	{
    	 if(bytes % 4 == 0)
             return bytes;
		 else
		return bytes + (4 - bytes % 4);
	}

	void Push(mslAllocator* allocator, uint32_t bytes)
	{
        allocator->WritePointer += Align(bytes)/4;
	}
    void Set(mslAllocator* allocator, mslAllocationHeader* header)
    {
        allocator->WritePointer = (uint32*)header;
    }

	mslAllocationHeader* FindFirstFree(mslAllocator* allocator, uint32 blockSize)
	{
		mslAllocationHeader* result = (mslAllocationHeader*)allocator->Start;
		ASSERT(result->CorruptionGuard == MSL_CORRUPTIONGUARD, "Memorycorruptionguard triggerd, memoryheader has been overwriten");

        while(result->NextBlock != 0)
        {
            ASSERT(result->CorruptionGuard == MSL_CORRUPTIONGUARD, "Memorycorruptionguard triggerd, memoryheader has been overwriten");
            if (result->State == mslAllocationStates::Free && result->BlockSize + result->FragmentedSpace >= blockSize) return result;
            result = result->NextBlock;
        }

        return result;
	}

    void MergeFreeBlocks(mslAllocator* allocator, mslAllocationHeader* freeHeader)
    {
        if(freeHeader->State != mslAllocationStates::Free) return;

        mslAllocationHeader* check = freeHeader;
        while(check->State == mslAllocationStates::Free)
        {
            ASSERT(check->CorruptionGuard == MSL_CORRUPTIONGUARD, "Memorycorruptionguard triggerd, memoryheader has been overwriten");
            if(check->PreviousBlock == 0) break;
            if (check->PreviousBlock->State != mslAllocationStates::Free) break;
            check = check->PreviousBlock;
        }
        mslAllocationHeader* first = check;
        while (check->State == mslAllocationStates::Free)
        {
            ASSERT(check->CorruptionGuard == MSL_CORRUPTIONGUARD, "Memorycorruptionguard triggerd, memoryheader has been overwriten");
            allocator->FragmentedSpace -= check->FragmentedSpace;
            check->FragmentedSpace = 0;
            if (check->NextBlock == 0) break;
            if (check->NextBlock->State != mslAllocationStates::Free) break;
            check = check->NextBlock;
        }
        // Merge first block to last free block
        if (first != check)
        {
            first->NextBlock = check->NextBlock;
            first->NextBlock->PreviousBlock = first;
            uint32 offset = (uint32)((uint8*)check - (uint8*)first) + check->BlockSize;
            first->BlockSize = offset;
        }

        // Compress
        if(first->NextBlock->State == mslAllocationStates::End)
        {
            first->State = mslAllocationStates::End;
            first->NextBlock = 0;
        }

    }
}



namespace MslAlloc
{
	mslAllocator Create(void* memory, uint32 size)
	{
		mslAllocator result = {};

		result.Size = size;
		result.Start = (uint32*)memory;
		result.WritePointer = result.Start;
		
        mslAllocationHeader* writeHeader = (mslAllocationHeader*)result.WritePointer;
		writeHeader->CorruptionGuard = MSL_CORRUPTIONGUARD;
		writeHeader->BlockSize = size - sizeof(mslAllocationHeader);
		writeHeader->PreviousBlock = 0;
		writeHeader->NextBlock = 0; 
        writeHeader->FragmentedSpace = 0;
		writeHeader->State = mslAllocationStates::End;
		writeHeader->Data = (void*)((int8*)result.WritePointer + sizeof(mslAllocationHeader)); 
		writeHeader->Identifyer = _MSL_GLOBAL_ALLOCINDEX++;
		
        result.UsedSpace += sizeof(mslAllocationHeader);

		return result;
	}

	mslMemBlock Alloc(mslAllocator* allocator, uint32 blockSize)
	{
        mslAllocationHeader* freeHeader = __MslAlloc::FindFirstFree(allocator, blockSize );
        ASSERT(freeHeader->CorruptionGuard == MSL_CORRUPTIONGUARD, "Memorycorruptiongurd triggerd, memoryheader has been overwriten");

        mslMemBlock result = {};
        result.BlockSize = blockSize;
        result.Data = freeHeader->Data;
        //uint32 fragmentedSpace = freeHeader->BlockSize - blockSize;
        freeHeader->BlockSize = blockSize;
        freeHeader->State = mslAllocationStates::Occupied;
        allocator->FragmentedSpace -= freeHeader->FragmentedSpace;

        if (freeHeader->NextBlock == 0)
        {
            __MslAlloc::Set(allocator, freeHeader);
            __MslAlloc::Push(allocator, blockSize + sizeof(mslAllocationHeader));
            mslAllocationHeader* writeHeader = (mslAllocationHeader*)allocator->WritePointer;

            writeHeader->CorruptionGuard = MSL_CORRUPTIONGUARD;
            uint32 offset = (uint32)((uint8*)allocator->WritePointer - (uint8*)allocator->Start);
            writeHeader->BlockSize = allocator->Size - (offset + sizeof(mslAllocationHeader));
            
            freeHeader->FragmentedSpace = 0;
            writeHeader->PreviousBlock = freeHeader;
            writeHeader->NextBlock = 0;
            writeHeader->State = mslAllocationStates::End;
            writeHeader->Data = (void*)((int8*)allocator->WritePointer + sizeof(mslAllocationHeader));
            writeHeader->FragmentedSpace = 0;
            writeHeader->Identifyer = _MSL_GLOBAL_ALLOCINDEX++;
            allocator->UsedSpace += sizeof(mslAllocationHeader) + blockSize;
            freeHeader->NextBlock = writeHeader;
        }
        else
        {
            freeHeader->FragmentedSpace = (uint32)((uint8*)freeHeader->NextBlock - (uint8*)freeHeader - blockSize - sizeof(mslAllocationHeader));

            if(freeHeader->FragmentedSpace > sizeof(mslAllocationHeader))
            {
                __MslAlloc::Set(allocator, freeHeader);
                __MslAlloc::Push(allocator, blockSize + sizeof(mslAllocationHeader));
                mslAllocationHeader* writeHeader = (mslAllocationHeader*)allocator->WritePointer;

                writeHeader->CorruptionGuard = MSL_CORRUPTIONGUARD;
                writeHeader->BlockSize = (uint32)((uint8*)freeHeader->NextBlock - (uint8*)writeHeader) - sizeof(mslAllocationHeader);//freeHeader->FragmentedSpace - sizeof(mslAllocationHeader);
                ASSERT(writeHeader->BlockSize <= 10240, "");

                writeHeader->FragmentedSpace = 0;
                writeHeader->PreviousBlock = freeHeader;
                writeHeader->NextBlock = freeHeader->NextBlock;
                freeHeader->NextBlock = writeHeader;
                writeHeader->NextBlock->PreviousBlock = writeHeader;
                writeHeader->State = mslAllocationStates::Free;
                writeHeader->Data = (void*)((int8*)allocator->WritePointer + sizeof(mslAllocationHeader));
                writeHeader->Identifyer = _MSL_GLOBAL_ALLOCINDEX++;
                
                freeHeader->FragmentedSpace = 0;

                // Merge past blocks
                __MslAlloc::MergeFreeBlocks(allocator, writeHeader);
            }
            else
            {
                allocator->FragmentedSpace += freeHeader->FragmentedSpace;
            }
        }
        
		return result;
	}

    void Free(mslAllocator* allocator, void* block)
    {
        // Assume block is aiming on first byte past blockheader
        mslAllocationHeader* header = (mslAllocationHeader*)((uint8*)block - sizeof(mslAllocationHeader));
        ASSERT(header->CorruptionGuard == MSL_CORRUPTIONGUARD, "Deallocating memory thats not a blockstart adress or invalid memory!");
        if (header->State == mslAllocationStates::Free) return;
        if (header->State == mslAllocationStates::End) return;

        header->State = mslAllocationStates::Free;
        allocator->UsedSpace -= header->BlockSize + sizeof(mslAllocationHeader);

        __MslAlloc::MergeFreeBlocks(allocator, header);
    }
}



#define _MSL_ALLOC_IMPLEMENTED_GUARD
#undef MSL_ALLOC_IMPLEMENTATION
#endif
#endif

/*
    Changelog

    231226  File created
        

    
*/
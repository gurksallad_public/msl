#ifndef MSL_ARRAY_H
#define MSL_ARRAY_H
//##########[ Header Implementation ]##########

#include "msl_defaults.h"



struct mslArray
{
    void            *Data;                                      //  Beginning of datablock
    uint32          Uid;                                        //  Unique number to identify this array (Arrays with the same uid are allowed to append to each other)
    uint32          Count;                                      //  Number of element array is currently holding
    uint32          PreallocatedCount;                          //  Maximum size this array can hold without resizing
    uint32          GrowSize;                                   //  Size array grows with everytim it needs to resize
    uint32          ElementSize;                                //  Size of each indiviual element
    bool            ReadOnly;                                   //  Array canot be added or removed to
};


namespace MslArray
{
    mslArray        Create(uint32 elementSize, uint32 growSize = 1, uint32 initialCount = 0, uint32 uid = 0);
    void*           Get(mslArray array, uint32 index);
    void            Add(mslArray array, void* element);
    void            AddRange(mslArray array, void* element, uint32 count);
    void            Remove(mslArray array, uint32 atIndex);
    void            RemoveRange(mslArray array, uint32 atIndex, uint32 count);
    mslArray        GetSlice(mslArray array, uint32 start, uint32 lenght);
}

#endif

#ifdef MSL_ARRAY_IMPLEMENTATION
#ifndef _MSL_ARRAY_IMPLEMENTED_GUARD
//##########[ Implementation ]##########


namespace MslArray
{
    //
    mslArray Create(uint32 elementSize, uint32 growSize = 1, uint32 initialCount = 0, uint32 uid = 0)
    {
        mslArray result = {};
        if(initialCount > 0) result.Data = malloc(initialCount * elementSize);
        result.Uid = uid;
        result.Count = 0;
        result.PreallocatedCount = initialCount;
        result.GrowSize = growSize;
        result.ElementSize = elementSize;
        result.ReadOnly = false;

        return result;
    }
}


#define _MSL_ARRAY_IMPLEMENTED_GUARD
#undef MSL_ARRAY_IMPLEMENTATION
#endif
#endif



/*
    Changelog

    240115  File created
        

    
*/
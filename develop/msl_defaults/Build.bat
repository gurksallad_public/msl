@echo off
cls
set arg1=%1

REM Create folders
IF NOT EXIST Build mkdir Build


REM Choose optimization
set optimization=-Od
IF "%arg1%"=="o" (
	set optimization=-O2
	echo Optimized Build
)

REM Buildoptions
set CommonCompilerFlags=-MD -nologo -fp:fast -Gm- -GR- -EHa- %optimization% -Oi -WX -W4 -wd4201 -wd4100 -wd4189 -wd4505 -FC -Zi 
set BuildMode=-DDEBUG_BUILD=1
set AppName=main
@REM set DllName=PetiteApi

REM Go to buildpath
pushd Build

REM build main application
echo Building main application
cl %CommonCompilerFlags% %BuildMode% ..\Source\%AppName%.cpp /link -incremental:no -opt:ref -PDB:Win32%AppName%.pdb

popd


REM run or debug application if specified
IF "%arg1%"=="r" (
	Run.bat
)
IF "%arg1%"=="d" (
	devenv /NoSplash /debugexe Build\win32%AppName%.exe
)
#include <stdio.h>

#define MSL_STRING_IMPLEMENTATION
#include "../../../include/msl_string.h"



int main(int argc, char *argv[])
{
    printf("Lib MslString\n\n");

    mslString string = {};

    int stringLenght = MslString::GetLenght("Hello mslStrings.h");
    printf("Lenght of string %u\n", stringLenght);


    bool stringSame = MslString::Compare("Yes", "no");
    printf("Is same? %u\n", stringSame);

    bool stringSame2 = MslString::Compare("Yes", "Yes");
    printf("Is same? %u\n", stringSame2);

    bool stringBeginsWith = MslString::BeginsWith("Yes, please!", "Yes");
    printf("Begins with? %u\n", stringBeginsWith);


    return 0;
}
#include "../../../include/msl_defaults.h"

#define MSL_ALLOC_IMPLEMENTATION
#include "../../../include/msl_alloc.h"
#include <stdio.h>
#include <iostream>
#include "imgui/imgui.h"
#include "render.cpp"


/*
Todos...
Make the allocator count allocaded space, allocated headers space and fragmented space
Make sure it does not allocat outside its bounds
Create som debug functions for checking memory corruptions, and other things???
Clean up the code
*/

mslAllocator allocator = {};
uint32* testdata;


uint32 rnd = 1000;

uint32 lcg_parkmiller(uint32 *state)
{
	return *state = (uint64)*state * 48271 % 0x7fffffff;
}

void TestAllocation()
{
    printf("Application running\n");

    testdata = (uint32*)malloc(200);

    int sysMemSize = 4 * 1024;
    void* sysMemory = malloc(sysMemSize);
    allocator = MslAlloc::Create(sysMemory, sysMemSize);

    //mslMemBlock block1 = MslAlloc::Alloc(&allocator, 128);
    //mslMemBlock block2 = MslAlloc::Alloc(&allocator, 156);
    //mslMemBlock block3 = MslAlloc::Alloc(&allocator, 212);
    //mslMemBlock block4 = MslAlloc::Alloc(&allocator, 96);
    //mslMemBlock block5 = MslAlloc::Alloc(&allocator, 512);

}


int allocationSize = 128;

void imgui()
{
    
    ImGui::Begin("Toolbox");
    ImGui::SliderInt("Size", &allocationSize,0, 512);
    if(ImGui::Button("Allocate"))
    {
        MslAlloc::Alloc(&allocator, allocationSize);
    }
    if(ImGui::Button("Random"))
    {
        if(lcg_parkmiller(&rnd)%2 == 0)
        {
            MslAlloc::Alloc(&allocator, lcg_parkmiller(&rnd) % 512);
        }
        else
        {
            mslAllocationHeader* h = (mslAllocationHeader*)allocator.Start;
            while(h != 0)
            {
                if(h->State == mslAllocationStates::Occupied)
                {
                    if(lcg_parkmiller(&rnd)%2 == 0)
                    {
                        MslAlloc::Free(&allocator, h->Data);
                        //break;
                    }
                }
                if(h->NextBlock == 0) break; 
                h = h->NextBlock;
            }
        }
    }
    ImGui::End();

    mslAllocationHeader* writeHeader = (mslAllocationHeader*)allocator.Start;
    ImGui::Begin("AllocationBlocks");
    {
        int index = 0;
        while(writeHeader != 0)
        {
            ImGui::SeparatorText("");
            char buffer[255];
            snprintf(buffer, 255, "BlockHeader %i", ++index);
            if (ImGui::TreeNode(buffer))
            {
                ImGui::Text("Size %i", writeHeader->BlockSize);
                ImGui::Text("Offset %i", (uint32)((int8*)writeHeader - (int8*)allocator.Start));
                ImGui::Text("DataSegment %i", (uint32)((int8*)writeHeader->Data - (int8*)writeHeader));
                ImGui::Text("ID: %i", writeHeader->Identifyer);
                
                if(writeHeader->State == mslAllocationStates::Free) ImGui::Text("State %s", "Free");
                if(writeHeader->State == mslAllocationStates::Occupied) ImGui::Text("State %s", "Occupied");
                if(writeHeader->State == mslAllocationStates::End) ImGui::Text("State %s", "Terminator");
                
                ImGui::Text("FragSpace %u", writeHeader->FragmentedSpace);
                if(writeHeader->State == mslAllocationStates::Occupied)
                {
                    if(ImGui::Button("Free"))
                    {
                        MslAlloc::Free(&allocator, writeHeader->Data);
                    }
                }
                ImGui::TreePop();
            }

            if (writeHeader->NextBlock == 0) break;
            writeHeader = writeHeader->NextBlock;
        }
    }
    ImGui::End();
}


// imcolor layout = 0xAABBGGRR
void Render(ImDrawList* draw_list, ImVec2 windowsOffset)
{
    ImVec2 screen_pos = ImGui::GetMousePos();
    screen_pos.x -= windowsOffset.x;
    screen_pos.y -= windowsOffset.y;

    char buffer[256];
    rd_offset = windowsOffset;
    rd_draw_list = draw_list;
    
    snprintf(buffer, 256, "Mouse: %f/%f", screen_pos.x, screen_pos.y);
    DrawText({ 1,1 }, buffer, 0xFF888888);

    float used = (float)allocator.UsedSpace / (float)allocator.Size;
    used = used * 776;
    DrawRect({10,10}, {780,50}, 0xFFFFFF00);
    //DrawFilledRect({12,12}, {12 + used,46}, 0xFF999900);


    snprintf(buffer, 256, "AllocationSize: %u bytes", allocator.Size);
    DrawText({ 600,70 }, buffer, 0xFF888888);

    snprintf(buffer, 256, "Size of header: %zu bytes", sizeof(mslAllocationHeader));
    DrawText({600,85}, buffer, 0xFF888888);

    snprintf(buffer, 256, "Allocated space: %u bytes", allocator.UsedSpace);
    DrawText({600,100}, buffer, 0xFF888888);

    snprintf(buffer, 256, "Allocated header: %u bytes", allocator.HeaderSpace);
    DrawText({600,115}, buffer, 0xFF888888);

    snprintf(buffer, 256, "Fragmented space: %u bytes", allocator.FragmentedSpace);
    DrawText({600,130}, buffer, 0xFF888888);

    mslAllocationHeader* mouseOver = 0;
    mslAllocationHeader* header = (mslAllocationHeader*)allocator.Start;

    while(header != 0)
    {
        float offset = (float)((int8*)header - (int8*)allocator.Start) / (float)allocator.Size;
        float offset2 = (float)(((int8*)header - (int8*)allocator.Start) + header->BlockSize + sizeof(mslAllocationHeader)) / (float)allocator.Size;
        if(screen_pos.x > 12 + (offset * 776) && screen_pos.x < 12 + (offset2 * 776) - 1 && screen_pos.y > 12 && screen_pos.y < 48)
        {
            mouseOver = header;
        }
        header = header->NextBlock;
    }

    mslAllocationHeader* writeHeader = (mslAllocationHeader*)allocator.Start;
    float ofs = 70;
    while(writeHeader != 0)
    {
        bool isOccupied = writeHeader->State == mslAllocationStates::Occupied ? true : false;
        float offset = (float)((int8*)writeHeader - (int8*)allocator.Start) / (float)allocator.Size;
        float offset2 = (float)(((int8*)writeHeader - (int8*)allocator.Start) + writeHeader->BlockSize + sizeof(mslAllocationHeader)) / (float)allocator.Size;
        float offset3 = (float)(((int8*)writeHeader - (int8*)allocator.Start) + sizeof(mslAllocationHeader)) / (float)allocator.Size;

        if(writeHeader->State == mslAllocationStates::End)
        {
            DrawFilledRect({ 12 + (offset * 776),12 }, { 12 + (offset3 * 776) - 1, 48 }, 0xFF338888);
        }
        else
        {
            if(mouseOver == writeHeader)
            {
                DrawFilledRect({ 12 + (offset * 776),12 }, { 12 + (offset2 * 776) - 1, 68 }, isOccupied ? 0xFFFF9900 : 0xFF884400);
            }
            if (mouseOver != 0)
            {
                if (mouseOver->PreviousBlock == writeHeader)
                {
                    DrawFilledRect({ 12 + (offset * 776),2 }, { 12 + (offset2 * 776) - 1, 48 }, isOccupied ? 0xFFFF9900 : 0xFF884400);
                }
                if (mouseOver->NextBlock == writeHeader)
                {
                    DrawFilledRect({ 12 + (offset * 776),2 }, { 12 + (offset2 * 776) - 1, 48 }, isOccupied ? 0xFFFF9900 : 0xFF884400);
                }
            }
            DrawFilledRect({ 12 + (offset * 776),12 }, { 12 + (offset2 * 776) - 1, 48 }, isOccupied ? 0xFFFF9900 : 0xFF884400);
            DrawFilledRect({ 12 + (offset * 776),12 }, { 12 + (offset3 * 776) - 1, 48 }, isOccupied ? 0xFF331588 : 0xFF110844);
            DrawRect({ 12 + (offset * 776),12 }, { 12 + (offset2 * 776) - 1, 48 }, 0xFF888888);

            DrawRect({10,ofs}, {260,ofs + 80}, 0xFFFFFF00);
            snprintf(buffer, 256, "Size: %i bytes", writeHeader->BlockSize);
            DrawText({ 15,ofs + 5 }, buffer, 0xFF888888);

            snprintf(buffer, 256, "Offset: %i", (uint32)((int8*)writeHeader - (int8*)allocator.Start));
            DrawText({ 15,ofs + 20 }, buffer, 0xFF888888);

            snprintf(buffer, 256, "DataStart: %i", (uint32)((int8*)writeHeader->Data - (int8*)allocator.Start));
            DrawText({ 15,ofs + 35 }, buffer, 0xFF888888);
        }
        ofs += 90;
        if (writeHeader->NextBlock == 0) break;
        writeHeader = writeHeader->NextBlock;
    }
}

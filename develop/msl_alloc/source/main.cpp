#define SOKOL_IMPL
#define SOKOL_GLCORE33
#include "sokol/sokol_app.h"
#include "sokol/sokol_gfx.h"
#include "sokol/sokol_log.h"
#include "sokol/sokol_glue.h"
// #define GLFW_INCLUDE_NONE
// #include "GLFW/glfw3.h"
#include "imgui/imgui.cpp"
#include "imgui/imgui_draw.cpp"
#include "imgui/imgui_widgets.cpp"
#include "imgui/imgui_tables.cpp"
#include "imgui/imgui_demo.cpp"
#include "imgui/imgui.h"

#include "sokol/sokol_imgui.h"


#include "test.cpp"

static struct {
    sg_pass_action pass_action;
} state;



static void init(void) {
    
    TestAllocation();

    sg_desc desc= {};
    desc.context = sapp_sgcontext();
    desc.logger.func = slog_func;
    sg_setup(&desc);

    simgui_desc_t imguiDesc = {};
    simgui_setup(&imguiDesc);

    // initial clear color
    sg_pass_action pass = {};
    pass.colors->load_action = SG_LOADACTION_CLEAR;
    pass.colors->clear_value = { 0.0f, 0.05f, 0.1f, 1.0 };
    state.pass_action = pass;

}

static void frame(void) {
    simgui_frame_desc_t frame = {};

    frame.width = sapp_width();
    frame.height = sapp_height();
    frame.delta_time = sapp_frame_duration();
    frame.dpi_scale = sapp_dpi_scale();
    simgui_new_frame(&frame);

    /*=== UI CODE STARTS HERE ===*/
    imgui();
    bool isOpen = true;
    ImGui::SetNextWindowSizeConstraints(ImVec2(800, 480), ImVec2(FLT_MAX, FLT_MAX));
    ImGui::Begin("Allocation",&isOpen);
    ImDrawList* draw_list = ImGui::GetWindowDrawList();
    const ImVec2 p = ImGui::GetCursorScreenPos();
    // float x = p.x + 4.0f;
    // float y = p.y + 4.0f;
    // draw_list->AddLine(ImVec2(p.x, p.y), ImVec2(p.x+100, p.y+100), 0xFF0000FF, 1.0f);    
    // draw_list->AddRectFilled(ImVec2(p.x+20, p.y+10), ImVec2(p.x+120, p.y+40), 0xFFFF00FF);
    Render(draw_list, {p.x+4,p.y+4});

    ImGui::End();
        
    /*=== UI CODE ENDS HERE ===*/

    sg_begin_default_pass(&state.pass_action, sapp_width(), sapp_height());
    simgui_render();
    sg_end_pass();
    sg_commit();
}

static void cleanup(void) {
    simgui_shutdown();
    sg_shutdown();
}

static void event(const sapp_event* ev) {
    simgui_handle_event(ev);
}

sapp_desc sokol_main(int argc, char* argv[]) {
    (void)argc;
    (void)argv;
    sapp_desc app = {};

    app.init_cb = init;
    app.frame_cb = frame;
    app.cleanup_cb = cleanup;
    app.event_cb = event;
    app.window_title = "Hello Sokol + Dear ImGui";
    app.width = 1280;
    app.height = 800;
    app.icon.sokol_default = true;
    app.logger.func = slog_func;
    return app;
}
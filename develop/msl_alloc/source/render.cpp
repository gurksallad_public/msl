
#include "imgui/imgui.h"


ImVec2 rd_offset = {0,0};
ImDrawList* rd_draw_list;




void DrawRect(ImVec2 position, ImVec2 size, ImU32 color)
{
    ImVec2 p = rd_offset;
    rd_draw_list->AddRect(ImVec2(p.x + position.x, p.y + position.y), ImVec2(p.x + size.x, p.y + size.y), color);
    
}

void DrawFilledRect(ImVec2 position, ImVec2 size, ImU32 color)
{
    ImVec2 p = rd_offset;
    rd_draw_list->AddRectFilled(ImVec2(p.x + position.x, p.y + position.y), ImVec2(p.x + size.x, p.y + size.y), color);
}

void DrawText(ImVec2 position, char* text, ImU32 color)
{
    ImVec2 p = rd_offset;
    rd_draw_list->AddText(ImVec2(p.x + position.x, p.y + position.y), color, text);
}
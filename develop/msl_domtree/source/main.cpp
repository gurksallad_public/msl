#include <stdio.h>

#define MSL_DOMTREE_IMPLEMENTATION
#include "../../../include/msl_domtree.h"
#include <string.h>



void ExtractNumbers(char* buffer, char* result, int lenght)
{
    char* read = buffer;
    char* write = result;
    int count = 0;

    while (*read != '\n' && *read != '\0')
    {
        if (*read >= 48 && *read <= 57)
        {
            *write = *read;
            write++;
            count++;
            if (count >= lenght - 1) break;
        }
        read++;
    }
    *write = 0;
}

int main(int argc, char *argv[])
{
    SetConsoleOutputCP(65001);

    // urldata dom = art_urlparser::DownloadUrl(argv[1]);
    mslUrldata dom1 = MslDomTree::DownloadUrl("https://www.bricklink.com/v2/catalog/catalogitem.page?S=8859-1#T=I");
    
    if(dom1.Error != 0) 
    {
        printf("Error loading url...\nQuiting");
        return 1;
    }
    
    mslDomtree tree1 = MslDomTree::ParseDom(dom1);
    
    mslQueryNode query = {};
    MslDomTree::QueryElement(tree1.Node, "<script>", &query);

    char partNumber[32];

    if (query.Found)
    {
        for (int i = 0; i < query.Count; ++i)
        {
            if (query.Node[i]->ChildNode != 0)
            {
                char* index = strstr(query.Node[i]->ChildNode->InnerText, "idItem:");
                if (index != 0)
                {

                    ExtractNumbers(index, partNumber, 32);
                    printf("Bricklink set id: %s\n", partNumber);
                }
            }
        }
    }

    char buffer[255];
    snprintf(buffer, 255, "https://www.bricklink.com/v2/catalog/catalogitem_invtab.page?idItem=%s&show_pcc=1", partNumber);
    mslUrldata dom2 = MslDomTree::DownloadUrl(buffer);
    
    if(dom2.Error != 0) 
    {
        printf("Error loading url...\nQuiting");
        return 1;
    }
    
    mslDomtree tree2 = MslDomTree::ParseDom(dom2);
    
    mslQueryNode queryRows = {};
    MslDomTree::QueryElement(tree2.Node, "<TR", &queryRows);

    if (queryRows.Found)
    {
        for (int i = 0; i < queryRows.Count; ++i)
        {
            char* index = strstr(queryRows.Node[i]->TagName, "pciinvItemRow");
            if (index != 0)
            {
                mslTreeNode* qtyNode = queryRows.Node[i]->ChildNode->NextSiblingNode->ChildNode->ChildNode->NextSiblingNode->NextSiblingNode->ChildNode;
                mslTreeNode* idNode = qtyNode->ParentNode->NextSiblingNode->ChildNode->ChildNode;
                mslTreeNode* nameNode = qtyNode->ParentNode->NextSiblingNode->NextSiblingNode->ChildNode->ChildNode;
                mslTreeNode* colorNode = nameNode->ParentNode->NextSiblingNode->ChildNode->NextSiblingNode->ChildNode->ChildNode;
                //ExtractNumbers(index, partNumber, 32);
                if (colorNode == 0)
                {
                    printf("%s  %s  %s\n", idNode->InnerText, qtyNode->InnerText, nameNode->InnerText);
                }
                else
                {
                    printf("%s  %s  %s    [%s]\n", idNode->InnerText, qtyNode->InnerText, nameNode->InnerText, colorNode->InnerText);
                }

            }
        }
    }


    return 0;
}